/// <reference types="node" />
declare const pipeBuffer: (buffer: Buffer, bin: string, args?: string[]) => Promise<Buffer>;
export default pipeBuffer;
