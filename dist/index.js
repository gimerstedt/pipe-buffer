var r=require("child_process");module.exports=((o,e,t=[])=>new Promise((n,s)=>{const d=[],a=r.spawn(e,t);a.on("error",r=>s(r)),a.stdout.on("error",r=>s(r)),a.stderr.on("data",r=>s(r)),a.stderr.on("error",r=>s(r)),a.stdin.on("error",r=>s(r)),a.stdout.on("data",r=>d.push(r)),a.stdout.on("end",()=>n(Buffer.concat(d))),a.stdin.write(o),a.stdin.end()}));
//# sourceMappingURL=index.js.map
