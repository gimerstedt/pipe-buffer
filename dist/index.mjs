import{spawn as r}from"child_process";export default(o,t,e=[])=>new Promise((n,d)=>{const s=[],i=r(t,e);i.on("error",r=>d(r)),i.stdout.on("error",r=>d(r)),i.stderr.on("data",r=>d(r)),i.stderr.on("error",r=>d(r)),i.stdin.on("error",r=>d(r)),i.stdout.on("data",r=>s.push(r)),i.stdout.on("end",()=>n(Buffer.concat(s))),i.stdin.write(o),i.stdin.end()});
//# sourceMappingURL=index.mjs.map
