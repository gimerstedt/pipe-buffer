# pipeBuffer
write a Buffer to stdin of a cli-app and get the resulting stdout as a Promise\<Buffer\>

## stupid example
``` typescript
const stdin = 'console.log(2 * 3 * 7)'
const buffer = Buffer.from(stdin)
const buf2str = (b: Buffer) => b.toString()
const str2int = (s: string) => parseInt(s.trim())
const is42 = (n: number) => n === 42

pipeBuffer(buffer, 'node')
  .then(buf2str)
  .then(str2int)
  .then(is42)
  .then(console.log) // true
```
