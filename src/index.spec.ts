import sut from './index'

describe('pipeBuffer', () => {
  it('resolves', async d => {
    const b1 = Buffer.from(`console.log('yup')`)
    const bin = 'node'

    const r1 = await sut(b1, bin)
    const r2 = await sut(b1, bin, ['-c'])

    expect(r1).toEqual(Buffer.from('yup\n'))
    expect(r1.toString().trim()).toEqual('yup')
    expect(r1.toString()).toEqual('yup\n')

    expect(r2.toString()).toEqual('')
    d()
  })

  it('rejects', async d => {
    try {
      await sut(Buffer.from('adsf'), 'qwer123fdsa')
      fail('should have thrown')
    } catch {}
    d()
  })
})
