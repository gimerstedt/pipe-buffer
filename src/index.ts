import { spawn } from 'child_process'

const pipeBuffer = (
  buffer: Buffer,
  bin: string,
  args: string[] = [],
): Promise<Buffer> => {
  return new Promise((resolve, reject) => {
    const buffers: Buffer[] = []
    const cp = spawn(bin, args)

    cp.on('error', e => reject(e))
    cp.stdout.on('error', e => reject(e))
    cp.stderr.on('data', e => reject(e))
    cp.stderr.on('error', e => reject(e))
    cp.stdin.on('error', e => reject(e))

    cp.stdout.on('data', b => buffers.push(b))
    cp.stdout.on('end', () => resolve(Buffer.concat(buffers)))

    cp.stdin.write(buffer)
    cp.stdin.end()
  })
}

export default pipeBuffer
